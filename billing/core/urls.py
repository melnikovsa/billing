from rest_framework import routers

from billing.core import views


def register_views(router):
    router.register(r'client', views.ClientViewSet)
    router.register(r'balance', views.BalanceViewSet)
    return router


urlpatterns = register_views(routers.SimpleRouter()).urls
