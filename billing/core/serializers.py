
from django.db import transaction
from django.db.models import F
from rest_framework import serializers

from billing.core import models


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Client
        fields = '__all__'


class DepositSerializer(serializers.ModelSerializer):
    client = serializers.CharField(required=True, max_length=50)
    amount = serializers.DecimalField(max_digits=24, decimal_places=8,  required=True)

    class Meta:
        model = models.Balance
        fields = ['client', 'amount']

    def validate(self, attrs):
        amount = attrs.get('amount')
        if amount <= 0:
            raise serializers.ValidationError(f'Amount must be greater than 0.')

        currency = models.Currency.objects.get(symbol='USD')

        client_name = attrs.get('client')
        client = models.Client.objects.filter(name=client_name).first()
        if not client:
            raise serializers.ValidationError(f'Client {client_name} not found.')

        wallet = models.Wallet.objects.get(client=client)

        return {
            'wallet': wallet,
            'currency': currency,
            'amount': amount,
        }

    @transaction.atomic()
    def create(self, validated_data):
        balance, _ = models.Balance.objects.get_or_create(
            wallet=validated_data.get('wallet'),
            currency=validated_data.get('currency'),
        )

        models.Balance.objects\
            .filter(id=balance.id)\
            .update(amount=F('amount') + validated_data.get('amount'))

        models.Transfer.objects.create(
            wallet_to=validated_data.get('wallet'),
            currency=validated_data.get('currency'),
            operation_type=models.Transfer.OperationType.DEPOSIT,
            amount=validated_data.get('amount'),
        )

        return models.Balance.objects.get(id=balance.id)


class TransferSerializer(serializers.ModelSerializer):
    client_from = serializers.CharField(required=True, max_length=50)
    client_to = serializers.CharField(required=True, max_length=50)
    amount = serializers.DecimalField(max_digits=24, decimal_places=8,  required=True)

    class Meta:
        model = models.Transfer
        fields = ['client_from', 'client_to', 'amount']

    def validate(self, attrs):
        amount = attrs.get('amount')
        if amount <= 0:
            raise serializers.ValidationError(f'Amount must be greater than 0.')

        client_from_name = attrs.get('client_from')
        client_to_name = attrs.get('client_to')

        if client_from_name == client_to_name:
            raise serializers.ValidationError(f'Clients must be different.')

        currency = models.Currency.objects.get(symbol='USD')

        client_from = models.Client.objects.filter(name=client_from_name).first()
        if not client_from:
            raise serializers.ValidationError(f'Client {client_from_name} not found.')

        wallet_from = models.Wallet.objects.get(client=client_from)

        balance_from, created = models.Balance.objects.get_or_create(
            wallet=wallet_from,
            currency=currency,
        )

        if created or balance_from.amount < amount:
            raise serializers.ValidationError(f'Not enough money on your wallet.')

        client_to = models.Client.objects.filter(name=client_to_name).first()
        if not client_to:
            raise serializers.ValidationError(f'Client {client_to_name} not found.')

        wallet_to = models.Wallet.objects.get(client=client_to)

        return {
            'wallet_from': wallet_from,
            'wallet_to': wallet_to,
            'currency': currency,
            'amount': amount,
        }

    @transaction.atomic()
    def create(self, validated_data):
        balance_from, _ = models.Balance.objects.get_or_create(
            wallet=validated_data.get('wallet_from'),
            currency=validated_data.get('currency'),
        )

        balance_to, _ = models.Balance.objects.get_or_create(
            wallet=validated_data.get('wallet_to'),
            currency=validated_data.get('currency'),
        )

        models.Balance.objects\
            .filter(id=balance_from.id)\
            .update(amount=F('amount') - validated_data.get('amount'))

        models.Balance.objects \
            .filter(id=balance_to.id) \
            .update(amount=F('amount') + validated_data.get('amount'))

        transfer = models.Transfer.objects.create(
            wallet_from=validated_data.get('wallet_from'),
            wallet_to=validated_data.get('wallet_to'),
            currency=validated_data.get('currency'),
            operation_type=models.Transfer.OperationType.TRANSFER,
            amount=validated_data.get('amount'),
        )

        return transfer
