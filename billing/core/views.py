from django.db import IntegrityError
from rest_framework import decorators
from rest_framework import mixins
from rest_framework import response
from rest_framework import status
from rest_framework import viewsets

from billing.core import models
from billing.core import serializers


class PostOnlyViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    ...


class ClientViewSet(PostOnlyViewSet):
    """The ViewSet for work with clients.  """

    serializer_class = serializers.ClientSerializer
    queryset = models.Client.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        client = serializer.save()

        # create default wallet here
        wallet = models.Wallet(client=client, name='default')
        wallet.save()

        response_data = {
            'client': self.serializer_class(instance=client).data
        }
        return response.Response(data=response_data, status=status.HTTP_201_CREATED)


class BalanceViewSet(viewsets.GenericViewSet):
    """The ViewSet for work with wallet balances."""

    queryset = models.Balance.objects.all()

    @decorators.action(detail=False, methods=['post'], serializer_class=serializers.DepositSerializer)
    def deposit(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return response.Response(data=request.data, status=status.HTTP_200_OK)

    @decorators.action(detail=False, methods=['post'], serializer_class=serializers.TransferSerializer)
    def transfer(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            serializer.save()
        except IntegrityError:
            return response.Response(data={'error': 'Something went wrong'}, status=status.HTTP_400_BAD_REQUEST)

        return response.Response(data=request.data, status=status.HTTP_200_OK)
