
from django.db import models


class Client(models.Model):
    """ Модель клиента"""
    name = models.CharField(max_length=50, unique=True)


class Currency(models.Model):
    """ Модель возможных валют в системе"""
    symbol = models.CharField(max_length=50, unique=True)


class Wallet(models.Model):
    """ Модель кошельков клиента"""
    client = models.OneToOneField(to=Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    class Meta:
        unique_together = (('client', 'name'),)


class Balance(models.Model):
    """ Модель баланса средств на кошельке"""
    wallet = models.ForeignKey(to=Wallet, on_delete=models.CASCADE)
    currency = models.ForeignKey(to=Currency, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=24, decimal_places=8,  default=0)

    class Meta:
        unique_together = (('wallet', 'currency'),)

        constraints = [
            models.CheckConstraint(check=models.Q(amount__gte=0), name='positive_amount'),
        ]


class Transfer(models.Model):
    """ Модель истории переводов денежных средств"""
    class OperationType(models.TextChoices):
        DEPOSIT = 'deposit'
        TRANSFER = 'transfer'

    wallet_from = models.ForeignKey(to=Wallet, on_delete=models.CASCADE, related_name='transfer_from', null=True)
    wallet_to = models.ForeignKey(to=Wallet, on_delete=models.CASCADE, related_name='transfer_to')
    currency = models.ForeignKey(to=Currency, on_delete=models.CASCADE)
    operation_type = models.CharField(max_length=10, choices=OperationType.choices)
    amount = models.DecimalField(max_digits=24, decimal_places=8)
    dt = models.DateTimeField(auto_now_add=True)
