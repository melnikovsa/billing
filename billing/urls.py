
from django import urls
from drf_yasg import openapi
from drf_yasg import views as drf_yasg_views
from rest_framework import permissions

from billing.core import urls as core_urls

SchemaView = drf_yasg_views.get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    urls.path('api/', urls.include(core_urls.urlpatterns)),
    urls.re_path(
        r"^swagger/$",
        SchemaView.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
]
