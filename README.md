# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

Simple billing system
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Install docker and docker-compose
* Configuration

`copy .env.example .env`
* Dependencies

All python packages store into requirements.txt and will be installed on docker-compose up command (on build only)
* Database configuration

If need change DB configs into .env and docker-compose.yml files 
* How to run tests

`docker exec -it django_container_name pytest`
* Deployment instructions

`run docker-compose up`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* 
+6Other community or team contact


### API format ###
Api format available at url http://127.0.0.1:8000/swagger/