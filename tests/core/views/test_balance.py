from decimal import Decimal

import pytest
import validx
from django import urls

from billing.core import models


class TestDeposit:
    deposit_response = validx.Dict({
        'client': validx.Str(minlen=1),
        'amount': validx.Float(min=0, coerce=True),
    })

    @pytest.mark.django_db
    @pytest.mark.usefixtures('clear_db')
    def test_deposit_success(
            self,
            client,
            client_1,
            client_1_default_wallet,
            usd_currency,
            client_1_wallet_1_usd_balance,
    ):
        deposit_amount = Decimal('1.1')
        response = client.post(
            path=urls.reverse('balance-deposit'),
            data={
                'client': client_1.name,
                'amount': str(deposit_amount)
            },
        )
        assert response.status_code == 200
        result = response.json()
        self.deposit_response(result)

        new_balance = models.Balance.objects.get(id=client_1_wallet_1_usd_balance.id)
        assert new_balance.amount == client_1_wallet_1_usd_balance.amount + deposit_amount

        transfer_log = models.Transfer.objects.all().first()
        assert transfer_log
        assert transfer_log.wallet_from is None
        assert transfer_log.wallet_to_id == client_1_default_wallet.id
        assert transfer_log.currency_id == usd_currency.id
        assert transfer_log.operation_type == models.Transfer.OperationType.DEPOSIT
        assert transfer_log.amount == deposit_amount

    @pytest.mark.django_db
    @pytest.mark.usefixtures('usd_currency', 'client_1_default_wallet', 'clear_db')
    def test_deposit_wrong_client(self, client, client_1):
        response = client.post(
            path=urls.reverse('balance-deposit'),
            data={
                'client': 'undefined',
                'amount': 1
            },
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    @pytest.mark.usefixtures('usd_currency', 'client_1_default_wallet', 'clear_db')
    @pytest.mark.parametrize(
        'amount',
        [
            (-1, ),
            ('aaa', ),
        ],
        ids=[
            'error: negative amount',
            'error: wrong amount type',
        ],
    )
    def test_deposit_wrong_amount(self, client, client_1, amount):
        response = client.post(
            path=urls.reverse('balance-deposit'),
            data={
                'client': client_1.name,
                'amount': amount
            },
        )
        assert response.status_code == 400


@pytest.fixture
def get_client_1_name(client_1):
    yield client_1.name


@pytest.fixture
def get_client_2_name(client_2):
    yield client_2.name


class TestTransfer:
    transfer_response = validx.Dict({
        'client_from': validx.Str(minlen=1),
        'client_to': validx.Str(minlen=1),
        'amount': validx.Float(min=0, coerce=True),
    })

    @pytest.mark.django_db
    @pytest.mark.usefixtures('clear_db')
    def test_transfer_success(
            self,
            client,
            client_1,
            client_2,
            client_1_default_wallet,
            client_2_default_wallet,
            usd_currency,
            client_1_wallet_1_usd_balance,
    ):
        transfer_amount = Decimal('1.1')
        response = client.post(
            path=urls.reverse('balance-transfer'),
            data={
                'client_from': client_1.name,
                'client_to': client_2.name,
                'amount': str(transfer_amount)
            },
        )
        assert response.status_code == 200
        result = response.json()
        self.transfer_response(result)

        new_balance = models.Balance.objects.get(id=client_1_wallet_1_usd_balance.id)
        assert new_balance.amount == client_1_wallet_1_usd_balance.amount - transfer_amount

        new_balance = models.Balance.objects.get(wallet=client_2_default_wallet, currency=usd_currency)
        assert new_balance.amount == transfer_amount

        transfer_log = models.Transfer.objects.all().first()
        assert transfer_log
        assert transfer_log.wallet_from_id == client_1_default_wallet.id
        assert transfer_log.wallet_to_id == client_2_default_wallet.id
        assert transfer_log.currency_id == usd_currency.id
        assert transfer_log.operation_type == models.Transfer.OperationType.TRANSFER
        assert transfer_log.amount == transfer_amount

    @pytest.mark.django_db
    @pytest.mark.usefixtures(
        'client_1_wallet_1_usd_balance',
        'clear_db',
    )
    @pytest.mark.parametrize(
        'client_from, client_to, amount',
        [
            ('aaa', get_client_2_name, 1),
            (get_client_1_name, 'aaa', 1),
            (get_client_1_name, get_client_1_name, 1),
            (get_client_1_name, get_client_2_name, 'aaa'),
            (get_client_1_name, get_client_2_name, -1),
            (get_client_1_name, get_client_2_name, 1000000),
        ],
        ids=[
            'error: wrong client_from',
            'error: wrong client_to',
            'error: same clients',
            'error: wrong amount type',
            'error: negative amount',
            'error: not enough money',
        ],
    )
    def test_transfer_wrong_data(self, client, client_from, client_to, amount):
        response = client.post(
            path=urls.reverse('balance-transfer'),
            data={
                'client_from': client_from,
                'client_to': client_from,
                'amount': amount
            },
        )
        assert response.status_code == 400
