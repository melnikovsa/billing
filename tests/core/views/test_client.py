import pytest
import validx
from django import urls


class TestClientViewSet:
    create_response = validx.Dict({
        'client': validx.Dict({
            'id': validx.Int(min=0),
            'name': validx.Str(minlen=0),
        }),
    })

    @pytest.mark.django_db
    @pytest.mark.usefixtures('usd_currency', 'clear_db')
    def test_create_client_success(self, client):
        response = client.post(
            path=urls.reverse('client-list'),
            data={
                'name': 'c1'
            },
        )
        assert response.status_code == 201
        result = response.json()
        self.create_response(result)

    @pytest.mark.django_db
    @pytest.mark.usefixtures('usd_currency', 'clear_db')
    def test_create_client_empty_name(self, client):
        response = client.post(
            path=urls.reverse('client-list'),
            data={
                'name': ''
            },
        )
        assert response.status_code == 400

    @pytest.mark.django_db
    @pytest.mark.usefixtures('usd_currency', 'clear_db')
    def test_create_client_existing_name(self, client, client_1):
        response = client.post(
            path=urls.reverse('client-list'),
            data={
                'name': client_1.name
            },
        )
        assert response.status_code == 400

