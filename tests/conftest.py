# pylint: disable=redefined-outer-name
# pylint: disable=invalid-name

from rest_framework import test as drf_test
import dotenv
import pytest

from billing.core import models


def pytest_addoption(parser):
    parser.addoption(
        '--integration',
        action='store_true',
        default='',
        help='Run integration tests with main test suite.',
    )


def pytest_configure(config):  # pylint: disable=unused-argument
    """Pytest hook that called before test session.

    Docs:
        https://docs.pytest.org/en/latest/reference.html#_pytest.hookspec.pytest_configure

    Args:
        config: Pytest config object.
    """
    dotenv.load_dotenv()


@pytest.fixture
def client():
    api_client = drf_test.APIClient()
    return api_client


@pytest.fixture
def clear_db():
    yield
    models.Client.objects.all().delete()


@pytest.fixture
def usd_currency():
    currency, _ = models.Currency.objects.get_or_create(symbol='USD')
    yield currency
    currency.delete()


@pytest.fixture
def client_1():
    client, _ = models.Client.objects.get_or_create(name='c1')
    yield client
    client.delete()


@pytest.fixture
def client_1_default_wallet(client_1):
    wallet, _ = models.Wallet.objects.get_or_create(client=client_1, name='default')
    yield wallet
    wallet.delete()


@pytest.fixture
def client_1_wallet_1_usd_balance(client_1_default_wallet, usd_currency):
    balance, _ = models.Balance.objects.get_or_create(
        wallet=client_1_default_wallet,
        currency=usd_currency,
        amount=100,
    )
    yield balance
    balance.delete()


@pytest.fixture
def client_2():
    client, _ = models.Client.objects.get_or_create(name='c2')
    yield client
    client.delete()


@pytest.fixture
def client_2_default_wallet(client_2):
    wallet, _ = models.Wallet.objects.get_or_create(client=client_2, name='default')
    yield wallet
    wallet.delete()

